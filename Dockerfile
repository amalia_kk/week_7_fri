# We want a Redhat-CentOS httpd server


# First we want to get a base contsiner with Redhat CentOS installed
# FROM redhat/ubi8-minimal:latest
FROM centos:centos7.9.2009

# Also need to run a command to install httpd
RUN yum -y install httpd

# Add our own content to the webpage
COPY index.html /var/www/html


# Then we want to explicitly mark port 80 as exposed - more for kubernetes and docker compose that needs to know where services are running
EXPOSE 80


# Lastly, we want a command so that httpd can automatically start when httpd every time the container is launched
# Entrypoint is like user data for AMIs
ENTRYPOINT ["httpd", "-DFOREGROUND"]

